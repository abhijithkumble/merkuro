# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kalendar package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kalendar\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-28 00:49+0000\n"
"PO-Revision-Date: 2023-09-16 11:02+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 23.04.3\n"

#: package/contents/ui/ContactPage.qml:49
#, kde-format
msgid "Return to Contact List"
msgstr "Wróć do spisu kontaktów"

#: package/contents/ui/ContactPage.qml:56
#: package/contents/ui/ContactPage.qml:57
#: package/contents/ui/ContactPage.qml:244
#, kde-format
msgid "Call"
msgstr "Zadzwoń"

#: package/contents/ui/ContactPage.qml:69
#: package/contents/ui/ContactPage.qml:101
#, kde-format
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/ContactPage.qml:88
#: package/contents/ui/ContactPage.qml:89
#, kde-format
msgid "Send SMS"
msgstr "Wyślij SMSa"

#: package/contents/ui/ContactPage.qml:119
#: package/contents/ui/ContactPage.qml:120
#: package/contents/ui/ContactPage.qml:214
#, kde-format
msgid "Send Email"
msgstr "Wyślij wiadomość"

#: package/contents/ui/ContactPage.qml:128
#: package/contents/ui/ContactPage.qml:129
#, kde-format
msgid "Show QR Code"
msgstr "Pokaż kod QR"

#: package/contents/ui/ContactPage.qml:159
#, kde-format
msgid "Nickname: %1"
msgstr "Pseudonim: %1"

#: package/contents/ui/ContactPage.qml:169
#: package/contents/ui/ContactPage.qml:173
#, kde-format
msgid "Birthday:"
msgstr "Urodziny:"

#: package/contents/ui/ContactPage.qml:171
#, kde-format
msgctxt "Day month format"
msgid "dd.MM."
msgstr "dd.MM."

#: package/contents/ui/ContactPage.qml:182
#, kde-format
msgid "Address"
msgid_plural "Addresses"
msgstr[0] "Adres"
msgstr[1] "Adresy"
msgstr[2] "Adresy"

#: package/contents/ui/ContactPage.qml:193
#, kde-format
msgctxt "%1 is the type of the address, e.g. home, work, ..."
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ContactPage.qml:202
#, kde-format
msgid "Email Address"
msgid_plural "Email Addresses"
msgstr[0] "Adres e-mail"
msgstr[1] "Adresy e-mail"
msgstr[2] "Adresy e-mail"

#: package/contents/ui/ContactPage.qml:232
#, kde-format
msgid "Phone number"
msgid_plural "Phone numbers"
msgstr[0] "Numer telefonu"
msgstr[1] "Numery telefonów"
msgstr[2] "Numery telefonów"

#: package/contents/ui/ContactPage.qml:241
#, kde-format
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ContactsPage.qml:16
#, kde-format
msgid "Contacts"
msgstr "Kontakty"

#: package/contents/ui/main.qml:15 package/contents/ui/main.qml:31
#, kde-format
msgid "Contact"
msgstr "Kontakt"

#: package/contents/ui/QrCodePage.qml:21 package/contents/ui/QrCodePage.qml:52
#, kde-format
msgid "QR Code"
msgstr "Kod QR"

#: package/contents/ui/QrCodePage.qml:35
#, kde-format
msgid "Return to Contact"
msgstr "Wróć do kontaktu"

#: package/contents/ui/QrCodePage.qml:53
#, kde-format
msgid "Data Matrix"
msgstr "Macierz danych"

#: package/contents/ui/QrCodePage.qml:54
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: package/contents/ui/QrCodePage.qml:78
#, kde-format
msgid "Change the QR code type"
msgstr "Zmień rodzaj kodu QR"

#: package/contents/ui/QrCodePage.qml:102
#, kde-format
msgid "Creating QR code failed"
msgstr "Nie udało się utworzyć kodu QR"

#: package/contents/ui/QrCodePage.qml:111
#, kde-format
msgid "The QR code is too large to be displayed"
msgstr "Kod QR jest zbyt duży do wyświetlenia"
